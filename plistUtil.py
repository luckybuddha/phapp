#coding:utf-8
__author__ = 'guoxinjia'
__date__ = '2020/9/7'

import os,sys


class PlistUtil:
    def alter(self, destFilePath, fileNew, old_str, new_str):
        """
        替换文件中的字符串
        :param file:文件名
        :param old_str:就字符串
        :param new_str:新字符串
        :return:
        """
        file_data = ""
        with open(destFilePath, "r", encoding="utf-8") as f:
            for line in f:
                if old_str in line:
                    line = line.replace(old_str, new_str)
                file_data += line
        f.close()
        with open(fileNew, "w", encoding="utf-8") as f:
            f.write(file_data)
        f.close()

    def work(self,filePath="E:\phapp\phui_template.plist", fileNew="E:\phapp\phui_23.plist", jobName="phappios",buildNumber="23"):
        self.alter(destFilePath=filePath,fileNew=fileNew,old_str="jobname",new_str=jobName)
        self.alter(destFilePath=fileNew,fileNew=fileNew,old_str="buildnumber",new_str=buildNumber)



if __name__ == '__main__':
    # ar = PlistUtil()
    # ar.work()
    if len(sys.argv) == 4:
        file = str(sys.argv[1]).strip()
        jobName = str(sys.argv[2]).strip()
        buildNumber = str(sys.argv[3]).strip()
        ar = PlistUtil()
        if jobName =="phamcappios":
            fileNew = "amc_{0}.plist".format(buildNumber)
        else:
            fileNew = "phui_{0}.plist".format(buildNumber)

        ar.work(filePath=file,fileNew=fileNew,jobName=jobName,buildNumber=buildNumber)
    else:
        print("params is not correct")
